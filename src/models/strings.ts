import { Language } from "./languages";

export const STRINGS: Record<string, { [lang in Language]: string }> = {
  name: {
    [Language.ES]: "español",
    [Language.FR]: "français",
    [Language.CN]: "中文",
    [Language.KO]: "한국어",
  },
  prompt_write: {
    [Language.ES]: "Escribe ingles aquí",
    [Language.FR]: "Écrivez anglais ici",
    [Language.CN]: "在这里写英文",
    [Language.KO]: "여기에 영어를 쓰세요",
  },
  prompt_translate: {
    [Language.ES]: "Escribe español aquí para traducir",
    [Language.FR]: "Écrivez français ici pour traduire",
    [Language.CN]: "在这里写中文来翻译",
    [Language.KO]: "번역하려면 여기에 한국어를 입력하세요",
  },
  translate: {
    [Language.ES]: "Traducir",
    [Language.FR]: "Traduire",
    [Language.CN]: "翻译",
    [Language.KO]: "번역하다",
  },
  language: {
    [Language.ES]: "Idiomas",
    [Language.FR]: "Langues",
    [Language.CN]: "语言",
    [Language.KO]: "언어",
  },
  words: {
    [Language.ES]: " palabras",
    [Language.FR]: " mots",
    [Language.CN]: "个字",
    [Language.KO]: "단어",
  },
  copy_success: {
    [Language.ES]: "copió",
    [Language.FR]: "copié",
    [Language.CN]: "复制了",
    [Language.KO]: "복사했습니다",
  },
  copy_failed: {
    [Language.ES]: "Ocurrió un problema al copiar la palabra.",
    [Language.FR]: "Un problème est survenu lors de la copie du mot.",
    [Language.CN]: "复制单词时出现问题。",
    [Language.KO]: "단어를 복사하는 중에 문제가 발생했습니다.",
  },
};
