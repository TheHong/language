export enum Language { // ISO 639-1
  ES = "es",
  FR = "fr",
  CN = "zh-CN",
  KO = "ko",
}

export const LANGUAGES = Object.values(Language);
