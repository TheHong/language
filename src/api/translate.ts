import axios from "axios";

const GOOGLE_TRANSLATE_URL =
  "https://translate.googleapis.com/translate_a/single";

const postTranslate = async (
  text: string,
  sourceLang: string,
  targetLang: string
) => {
  const url = `${GOOGLE_TRANSLATE_URL}?client=gtx&sl=${sourceLang}&tl=${targetLang}&dt=t&q=${encodeURI(
    text
  )}`;
  const response = await axios.post(url);
  return response;
};

export const translate = async (
  text: string,
  sourceLang: string,
  targetLang: string
) => {
  const response = await postTranslate(text, sourceLang, targetLang);
  return response.data[0][0][0];
};
